package com.rifqidmw.workmanager

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.work.Constraints
import androidx.work.NetworkType
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val constraint = Constraints.Builder()
            .setRequiresCharging(true)
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

        val request = OneTimeWorkRequestBuilder<MyWork>()
            .setConstraints(constraint)
            .build()

        btn_click.setOnClickListener {
            WorkManager.getInstance(this@MainActivity).enqueue(request)
        }

        WorkManager.getInstance(this@MainActivity).getWorkInfoByIdLiveData(request.id)
            .observe(this, Observer {
                val status: String = it.state.name
                Toast.makeText(this@MainActivity, status, Toast.LENGTH_SHORT).show()
            })
    }
}